#include<stdio.h>
#include<limits.h>
int main() {
    printf("char     %lu\n", sizeof(char));
    printf("int      %lu\n", sizeof(int));
    printf("long     %lu\n", sizeof(long));
    printf("int      %lu\n", sizeof(long long));

    //puntos flotantes
    printf("float           %lu\n", sizeof(float));
    printf("double          %lu\n", sizeof(double));
    printf("long double     %lu\n", sizeof( long double));
}
